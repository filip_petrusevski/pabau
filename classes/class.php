<?php

class Main
{
    private $db;
    function __construct($db)
    {
        $this->_db = $db;
    }
    public function login()
    {
        if (isset($_POST['submit'])) {
            $email = addslashes(strip_tags($_POST['email']));
            $password = addslashes(strip_tags($_POST['password']));
            if (!empty($email) && !empty($password)) {
                $sql = $this->_db->prepare("SELECT * FROM `patients` WHERE `email`=:email AND `password`=:password");
                $sql->execute(array('email' => $email, 'password' => $password));
                if ($sql->rowCount()) {
                    $data = $sql->fetch();
                    $_SESSION['id'] = $data['id'];
                    $_SESSION['id'] = true;
                    header('location:../dashboard.php');
                } else {
                    echo "Email or Password are wrong";
                }
            } else {
                echo " Please enter email and password";
            }
        }
    }
}
