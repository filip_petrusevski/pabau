<?php

class DB
{
    protected function connect()
    {
        try {
            $pdo = new PDO("mysql:host=localhost;dbname=pabau", 'root', '');
            return $pdo;
        } catch (PDOException $e) {
            print "ERROR:  " . $e->getMessage() . "</br>";
            die();
        }
    }
}
